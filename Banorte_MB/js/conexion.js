var socket;
var started = false;

function commandCard(buttonAction){

  if (buttonAction != "CONNECT" && !started) {
    $("#output").append('<br><span style="color:red;">Debe iniciar una conexión</span>');
  }

  switch(buttonAction) {
    case "CONNECT":
      started = true;
      socket = new WebSocket("ws://" +$("#server").val());
      socket.onopen = function() {
      	$("#output").append('<br><span style="color:white;">Conexión establecida con servidor</span>');
      }

      socket.onmessage = function(event) {
        processResponse(event.data);
        console.log(event.data);
      }

      socket.onclose = function() {
      	$("#output").append('<br><span style="color:white;">Conexión cerrada con servidor</span>');
      }

      socket.onerror = function(error) {
      	$("#output").append('<br><span style="color:red;">'+error+'</span>');
      }
    break;

    case "DISCONNECT":
      socket.close();
    break;

    case "INFO":
      socket.send("SYSTEM DEVICES LIST");
    break;

    case "START":
      socket.send("START " +$("#inputDevices").val());
    break;

    case "PROCESS":
      socket.send("PROCESS " +$("#inputDevices").val());
    break;

    case "COMMAND":
      socket.send("COMMAND " +$("#inputDevices").val() +" " +$("#inputText").val());
      $("#inputText").val('');
    break;
  }
}

function processResponse(response)
{
  var resp_json = JSON.parse(response);

  switch(resp_json.type) {
    case 0:
      $("#output").append('<br><span style="background-color:black;color:blue">' +resp_json.message +'</span>');
    break;
    case 1:
      $("#output").append('<br><span style="background-color:black;color:blue">' +resp_json.message +'</span>');
    break;
    case 2:
      $("#output").append('<br><span style="background-color:black;color:yellow">' +resp_json.message +'</span>');
    break;
  }

  switch(resp_json.type)
  {
    case 2:
      switch(resp_json.command) {
      	case "LIST":
      	case "CONNECT":
      	  $("#inputDevices").find('option').remove().end()
      	  	.append('<option selected>Seleccione ...</option>');
      	  var devices = resp_json.message.split(",");
      	  devices.forEach(function (device) {
      	    $("#inputDevices").append('<option value="'+device+'">'+device+'</option>');
      	  });
        break;
      }
    break;
  }
}
